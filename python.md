# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-uprev.venv
$ source ci-uprev.venv/bin/activate
(...)
$ python -m pip install python-gitlab gitpython
$ pip list
Package            Version
------------------ ---------
certifi            2022.9.14
charset-normalizer 2.1.1
gitdb              4.0.9
GitPython          3.1.27
idna               3.4
pip                22.2.2
python-gitlab      3.9.0
requests           2.28.1
requests-toolbelt  0.9.1
setuptools         65.1.1
smmap              5.0.0
urllib3            1.26.12
wheel              0.37.1
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze > requirements.txt
```
