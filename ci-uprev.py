#!/usr/bin/env python3.9

# Copyright (C) 2022 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022 Collabora Ltd"

from argparse import ArgumentParser
import base64
from collections import defaultdict, deque
from copy import deepcopy
import os
import re
import sys
import time
from typing import Tuple

import git
import gitlab
from gitlab.v4.objects import ProjectPipeline

# target means in this context the project to where the uprev is to be committed
# fork is the user's repo from where the MR will be created
# dep is the other project, the one referenced in the target project

TARGET_PROJECT_PATH = os.environ.get('TARGET_PROJECT_PATH', 'virgl/virglrenderer')
FORK_PROJECT_PATH = os.environ.get('FORK_PROJECT_PATH', 'sergi/virglrenderer')
DEP_PROJECT_PATH = os.environ.get('DEP_PROJECT_PATH', 'mesa/mesa')
UPREV_TMP_BRANCH = "mesa-uprev-tmp"   # Branch to which we first push to get the initial results
UPREV_WIP_BRANCH = "mesa-uprev-wip"   # Branch to which we push if there are possible regressions
UPREV_BRANCH = "mesa-uprev"           # Branch to which we push if there are only UnexpectedPasses

# return error codes
JOBS_WITHOUT_ARTIFACTS = -1

GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
if GITLAB_TOKEN is None:
    try:
        with open(os.path.expanduser('~/.gitlab-token')) as file_descriptor:
            GITLAB_TOKEN = file_descriptor.read().strip()
    except FileNotFoundError:
        raise AssertionError(f"Use the GITLAB_TOKEN environment variable or "
                             f"the ~/.gitlab-token to provide this information")

gl = gitlab.Gitlab(url='https://gitlab.freedesktop.org',
                   private_token=GITLAB_TOKEN)
gl.auth()
dep_project = gl.projects.get(DEP_PROJECT_PATH)
target_project = gl.projects.get(TARGET_PROJECT_PATH)
fork_project = gl.projects.get(FORK_PROJECT_PATH)


def get_candidate_for_uprev():
    pipelines = dep_project.pipelines.list(status='success',
                                           iterator=True,
                                           per_page=1,
                                           username="marge-bot")
    pipeline = pipelines.next()
    return pipeline.id, pipeline.sha

def get_templates_commit(revision):
    file = dep_project.files.get(".gitlab-ci.yml", ref=revision)
    file_content = base64.b64decode(file.content).decode("utf-8")
    for line in file_content.split('\n'):
        if "MESA_TEMPLATES_COMMIT" in line:
            # TODO: Use better a regex
            return line.split(" ")[4]

def create_branch(pipeline_id, revision, templates_commit):
    if os.path.exists("./target"):
        print(f"Reusing repo")
        repo = git.Repo("./target")
        repo.remote("origin").update()
        repo.remote("fork").update()

        if UPREV_TMP_BRANCH in repo.heads:
            print(f"Deleting existing branch")
            repo.heads["master"].checkout(force=True)
            repo.delete_head(UPREV_TMP_BRANCH, force=True)
    else:
        print(f"Cloning repo")
        repo = git.Repo.clone_from(f"https://gitlab.freedesktop.org/{TARGET_PROJECT_PATH}.git", "./target", branch='master')
        repo.config_writer().set_value("user", "name", "Collabora's Gfx CI Team").release()
        repo.config_writer().set_value("user", "email", "sergi.blanch.torne@collabora.com").release()
        remote = repo.create_remote("fork", url=f"https://sergi:{GITLAB_TOKEN}@gitlab.freedesktop.org/{FORK_PROJECT_PATH}.git")

    repo.heads["master"].checkout(force=True, b=UPREV_TMP_BRANCH)

    os.chdir(repo.working_dir)
    try:
        with open(".gitlab-ci.yml", "rt") as file_descriptor:
           contents = file_descriptor.readlines()
        in_mesa_include = False
        for i, line in enumerate(contents):
            if "MESA_PIPELINE_ID:" in line:
                # TODO: Would be better to use a regex to replace only the number, to preserve whitespace
                contents[i] = f"  MESA_PIPELINE_ID: {pipeline_id}\n"
            elif "project: " in line and "'mesa/mesa'" in line:
                in_mesa_include = True
            elif in_mesa_include and "ref:" in line:
                # TODO: Would be better to use a regex to replace only the sha, to preserve whitespace
                contents[i] = f"    ref: {revision}\n"
            elif "MESA_TEMPLATES_COMMIT:" in line:
                contents[i] = f"  MESA_TEMPLATES_COMMIT: &ci-templates-commit {templates_commit}\n"

        with open(".gitlab-ci.yml", "wt") as file_descriptor:
            file_descriptor.writelines(contents)

        index = repo.index # This is expensive, reuse the index object
        index.add(".gitlab-ci.yml")
        index.commit(f"Uprev Mesa to {revision}")

        print(f"Created commit with SHA {repo.head.commit.hexsha}")
    finally:
        os.chdir('..')

    return repo

def push(repo):
    remote = repo.remote("fork")
    remote.push(force=True)

    print(f"Waiting for pipeline to be created for {repo.head.commit.hexsha}")
    while True:
        pipelines = fork_project.pipelines.list(sha=repo.head.commit.hexsha)
        if pipelines:
            return pipelines[0]
        time.sleep(1)


def run(
        pipeline: ProjectPipeline
    ) -> dict:
    print(f"Triggering jobs for pipeline {pipeline.web_url}")
    # FIXME: Jobs are all initially in the "created" state, properly wait for them to come to the "manual" state
    time.sleep(1)
    for job in pipeline.jobs.list(all=True):
        if job.status == 'manual':
            pjob = fork_project.jobs.get(job.id, lazy=True)
            pjob.play()

    pipeline = __wait_pipeline_finished(pipeline)

    # check if there are results and they can be collected
    failures, artifacts, jobs_without_artifacts = collate_results(pipeline)
    if jobs_without_artifacts:
        pipeline, artifacts = __retry_jobs_without_artifacts(pipeline, artifacts, jobs_without_artifacts)
    if artifacts:
        __retry_flake_candidates(pipeline, failures, artifacts)

    return failures


def __wait_pipeline_finished(pipeline: ProjectPipeline) -> ProjectPipeline:
    """
    Periodically check if the pipeline has finished.
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    print(f"Wait for pipeline {pipeline.web_url} to finish")
    while True:
        pipelines = fork_project.pipelines.list(sha=pipeline.sha)
        if pipelines[0].status not in ["created", "waiting_for_resource",
                                       "preparing", "pending", "running"]:
            print(f"Pipeline {pipeline.web_url} finished")
            return pipeline
        time.sleep(10)


def __retry_jobs_without_artifacts(
        pipeline: ProjectPipeline,
        artifacts: dict,
        jobs_without_artifacts: list
    ) -> Tuple[ProjectPipeline, dict]:
    """
    if there are jobs without artifacts, lets give them another try
    :return:
    """
    print(f"Some jobs didn't produce artifacts")
    for job in jobs_without_artifacts:
        print(f"Retry '{job.name}' for artifacts build.")
        pjob = fork_project.jobs.get(job.id, lazy=True)
        pjob.retry()
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering artefacts already downloaded to avoid overwriting
    _, artifacts, jobs_without_artifacts = collate_results(pipeline, artifacts=artifacts)
    if len(jobs_without_artifacts) != 0:
        job_names = [job.name for job in jobs_without_artifacts]
        raise RuntimeError(f"These jobs failed to produce artifacts for a second time: {', '.join(job_names)}")
    return pipeline, artifacts


def __retry_flake_candidates(
        pipeline: ProjectPipeline,
        failures: dict,
        artifacts: dict
    ) -> Tuple[ProjectPipeline, dict, dict]:
    """
    Do one retry in the jobs that may have failed to
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline
    """
    for job in pipeline.jobs.list(iterator=True):
        if job.status == 'failed':
            print(f"Retry '{job.name}' to discard flakes")
            pjob = fork_project.jobs.get(job.id, lazy=True)
            pjob.retry()
    pipeline = __wait_pipeline_finished(pipeline)
    # collect results again, but:
    # - remembering failures to collect together new results
    # - remembering artefacts already downloaded to avoid duplication
    _, _, jobs_without_artifacts = collate_results(pipeline, failures=failures, artifacts=artifacts)
    if len(jobs_without_artifacts) != 0:
        pipeline, artifacts = __retry_jobs_without_artifacts(pipeline, artifacts, jobs_without_artifacts)
    failures, artifacts, _ = collate_results(pipeline, failures=failures, artifacts=artifacts, recollect=True)
    return pipeline, failures, artifacts


def collate_results(
        pipeline: ProjectPipeline,
        failures: dict = None,
        artifacts: dict = None,
        recollect: bool = False
        ) -> Tuple[dict, dict, list]:
    """
    Find and collect the artifacts of the jobs. Classify them in a dictionary
    or, if there isn't, collect those jobs in a list.
    :param pipeline: ProjectPipeline
    :param failures: dict
    :param artifacts: dict
    :param recollect: bool
    :return: failures, artifacts, without_artifacts
    """
    print(f"Looking for failed jobs in pipeline {pipeline.web_url}")

    def defaultdictdeque() -> defaultdict:
        return defaultdict(deque)

    if artifacts is None:
        artifacts = dict()
    if failures is None:
        # failures dict has as keys the tripplet (test_suite, backend, api)
        #  as value it has a nested dict where the key is the test_name and
        #  the values are deque of ens states of the tests in different reties.
        failures = defaultdict(defaultdictdeque)
    else:
        failures = deepcopy(failures)  # propagate will be decided by return
    test_suites = set()
    backends = set()
    apis = set()
    tests = set()
    without_artifacts = []

    for job in pipeline.jobs.list(per_page=100):
        job = fork_project.jobs.get(job.id)

        if job.status != 'failed':
            continue

        if job.stage in ['build', 'sanity test']:
            print("Something very bad happened: a build or sanity test job failed!")
            sys.exit(0)

        if 'piglit' in job.name:
            test_suite = 'piglit'
        elif 'traces' in job.name:
            test_suite = 'traces'
        elif 'deqp' in job.name:
            test_suite = 'deqp'
        else:
            print(f"Couldn't infer test suite from job name {job.name}!")
            sys.exit(0)
        test_suites.add(test_suite)

        if 'host' in job.name:
            backend = 'host'
        elif 'virt' in job.name or 'traces' in job.name:
            backend = 'virt'
        else:
            print(f"Couldn't infer backend from job name {job.name}!")
            sys.exit(0)
        backends.add(backend)

        if 'gles' in job.name:
            api = 'gles'
        elif 'gl' in job.name:
            api = 'gl'
        elif 'traces' in job.name:
            api = 'traces'
        else:
            print(f"Couldn't infer api from job name {job.name}!")
            sys.exit(0)
        apis.add(api)


        # TODO: handle deqp and trace jobs
        if backend == 'host':
            failures_path = f'results/{job.name}/failures.csv'
        elif backend == 'virt':
            failures_path = 'results/failures.csv'

        try:
            if job.name not in artifacts:
                print(f"Downloading artifacts for '{job.name}' {job.web_url}")
                artifacts[job.name] = job.artifact(failures_path)
            results = artifacts[job.name]
        except Exception as ex:
            print(f"An exception occurred when downloading results for '{job.name}' {job.web_url}: {ex}!")
            without_artifacts.append(job)
            continue

        for line in results.decode('utf-8').split('\n'):
            if not line.strip():
                continue
            test_name, result = line.split(',')
            tests.add(test_name)
            # detect flakiness: collect the current execution results
            failures[(test_suite, backend, api)][test_name].append(result)
            if recollect and len(failures[(test_suite, backend, api)][test_name]) == 1:
                # As we only collect when fail is reported or unexpected pass,
                #  complete the information
                if result == "UnexpectedPass":
                    failures[(test_suite, backend, api)][test_name].appendleft("ExpectedFail")
                else:
                    failures[(test_suite, backend, api)][test_name].appendleft("Pass")

    if recollect:  # review and complete the list of pairs with the implicit pass
        for test_dict in failures.values():
            for results_deque in test_dict.values():
                if len(results_deque) == 1:
                    if results_deque[0] == "UnexpectedPass":
                        results_deque.append("ExpectedFail")
                    else:
                        results_deque.append("Pass")

    return failures, artifacts, without_artifacts


def update_branch(repo, failures):
    os.chdir(repo.working_dir)
    try:
        index = repo.index # This is expensive, reuse the index object
        possible_regressions = False
        possible_flakes = False
        for job_fields, test_dict in failures.items():
            test_suite, backend, api = job_fields

            expectations_path = f".gitlab-ci/expectations/{backend}"
            fails_file_name = f"{expectations_path}/virgl-{api}-fails.txt"
            with open(fails_file_name, "rt") as file_descriptor:
                fails_contents = file_descriptor.readlines()
            flakes_file_name = f"{expectations_path}/virgl-{api}-flakes.txt"
            with open(flakes_file_name, "rt") as file_descriptor:
                flakes_contents = file_descriptor.readlines()
            fixed_tests = []

            for test_name, test_results in test_dict.items():
                # remove duplicated elements
                test_results_unique = set(test_results)
                assert len(test_results_unique) > 0, "Failed, tests should have at least one result"
                if len(test_results_unique) != 1:
                    print(f"Flaky test detected in "
                          f"({test_suite}, {backend}, {api}) "
                          f"{test_name=} with results {list(test_results)}")
                    flakes_contents += [f"{test_name}\n"]
                    possible_flakes = True
                elif test_results[0] == "UnexpectedPass":
                    print(f"UnexpectedPass test detected in "
                          f"({test_suite}, {backend}, {api}) "
                          f"{test_name=}")
                    fixed_tests += [test_name]
                else:
                    print(f"Failed test detected in "
                          f"({test_suite}, {backend}, {api}) "
                          f"{test_name=} with results {test_results[0]}")
                    fails_contents += [f"{test_name},{test_results[0]}\n"]
                    possible_regressions = True

            for test_name in fixed_tests:
                if f"{test_name},Fail\n" in fails_contents:
                    fails_contents.remove(f"{test_name},Fail\n")
                if f"{test_name},Crash\n" in fails_contents:
                    fails_contents.remove(f"{test_name},Crash\n")

            for file_name, contents in [(fails_file_name, fails_contents), (flakes_file_name, flakes_contents)]:
                with open(file_name, "wt") as file_descriptor:
                    file_descriptor.writelines(contents)
                print(f"Patched file {file_name}")
                index.add(file_name)

        repo.git.commit("--amend", "--no-edit")

        print(f"Amended commit with new SHA {repo.head.commit.hexsha}")

        if possible_regressions or possible_flakes:
            print(f"The pipeline requires manual intervention. So, using the {UPREV_WIP_BRANCH} branch.")
            repo.active_branch.rename(UPREV_WIP_BRANCH, force=True)
        else:
            repo.active_branch.rename(UPREV_BRANCH, force=True)
    finally:
        os.chdir('..')


def push_to_merge_request(repo, failures):
    remote = repo.remote("fork")
    remote.push(force=True)

    mr_title = '[TESTONLY] ci: Uprev Mesa to the latest version'
    regressions_per_category = defaultdict(dict)
    flake_per_category = defaultdict(dict)
    for job_fields, test_dict in failures.items():
        test_suite, backend, api = job_fields
        for test_name, results in test_dict.items():
            results_unique = set(results)
            if len(results_unique) != 1:
                category = f"Unreliable tests with the {backend} backend with {api}:\n"
                flake_per_category[category][test_name] = list(results)
            elif results[0] != "UnexpectedPass":
                category = f"Possible regressions with the {backend} backend with {api} on the host:\n"
                regressions_per_category[category][test_name] = results[0]

    regressions_comment = []
    for category, regression in regressions_per_category.items():
        regressions_comment += [category]
        for test_name, result in regression.items():
            regressions_comment += [f"- {test_name},{result}\n"]
        regressions_comment += ["\n"]
    flakes_comment = []
    for category, flaky in flake_per_category.items():
        flakes_comment += [category]
        for test_name, results in flaky.items():
            flakes_comment += [f"- {test_name}, {list(results)}\n"]
        flakes_comment += ["\n"]

    if regressions_comment or flakes_comment:
        print("Possible regressions found, marking MR as WIP")
        mr_title = "Draft: " + mr_title

    merge_request = None
    for mr in target_project.mergerequests.list(state='opened', iterator=True):
        if mr_title == mr.title and \
           mr.author["username"] == gl.user.username:
            merge_request = mr
            break

    if merge_request is None:
        if regressions_per_category or flake_per_category:
            branch = UPREV_WIP_BRANCH
        else:
            branch = UPREV_BRANCH

        mr = fork_project.mergerequests.create({'source_branch': branch,
                                                'target_branch': 'master',
                                                'target_project_id': target_project.id,
                                                'title': mr_title,
                                                'labels': ['ci']})
        print(f"Created merge request {mr.web_url}")
    else:
        print(f"Pushed to existing merge request {mr.web_url}")

    comments = ["".join(c) for c in [regressions_comment, flakes_comment] if c]
    body = "\n---\n".join(comments)
    mr.notes.create({"body": body})


def cli_arguments():
    parser = ArgumentParser(description="CLI tool to update the revision of a project used inside another project.")
    # TODO: mention that no argument is required
    #  all the configuration elements come from environment variables and this
    #  arguments are only for development and debugging.
    #  So, also show help about those envvars
    #   TARGET_PROJECT_PATH, FORK_PROJECT_PATH, DEP_PROJECT_PATH, GITLAB_TOKEN,
    #   REVISION
    parser.add_argument("--revision", help="Specify the revision to uprev")
    return parser.parse_args()


def main():
    args = cli_arguments()
    revision = os.environ.get('REVISION')

    if args.revision:
        revision = args.revision
    if revision:
        pipelines = dep_project.pipelines.list(sha=revision, state='success')
        if not pipelines:
            raise ValueError(f"No pipeline found with this revision")
        pipeline_id = pipelines[-1].id
        print(f"Specified the revision to uprev to {revision}")
    else:
        pipeline_id, revision = get_candidate_for_uprev()
        print(f"Found pipeline {pipeline_id} with SHA {revision}")

    templates_commit = get_templates_commit(revision)
    repo = create_branch(pipeline_id, revision, templates_commit)

    pipeline = push(repo)
    failures = run(pipeline)

    update_branch(repo, failures)

    push_to_merge_request(repo, failures)


if __name__ == '__main__':
    main()
