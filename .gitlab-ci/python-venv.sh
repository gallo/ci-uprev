#!/bin/bash

set -e

echo -e "\e[0Ksection_start:`date +%s`:python3[collapsed=true]\r\e[0KInstall python3, the corresponding pip and dependencies like git and ssh client"
export `cat /etc/os-release | grep ^ID=`
if [ $ID == 'alpine' ]; then
    apk add --update --no-cache python3 py3-pip git
elif [ $ID == 'fedora' ]; then
    dnf install -y python3 python3-pip git
fi
echo -e "\e[0Ksection_end:`date +%s`:python3\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:virtualenv[collapsed=true]\r\e[0KSetup virtual environment"
pip3 install virtualenv
python3 -m virtualenv ci-uprev.venv
source ci-uprev.venv/bin/activate
pip install -r requirements.txt
echo -e "\e[0Ksection_end:`date +%s`:virtualenv\r\e[0K"
