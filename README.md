This projects contains scripts and pipelines that automatically update dependencies used in CI systems such as that of Mesa and virglrenderer.

= Contributing =

Patches are submitted via a merge request registered at https://gitlab.freedesktop.org/gfx-ci/ci-uprev
